import { lexer, TokenType } from '../../src/utils/interpolation';

test('string without placeholders', () => {
  expect(lexer('simple')).toEqual([{ type: 'string', value: 'simple' }]);
});

test('string with placeholder in the end', () => {
  expect(lexer('simple${placeholderValue}')).toEqual([
    { type: TokenType.string, value: 'simple' },
    { type: TokenType.placeholder, value: 'placeholderValue' },
  ]);
});

test('string starts with placeholder', () => {
  expect(lexer('${placeholderValue}simple')).toEqual([
    { type: TokenType.placeholder, value: 'placeholderValue' },
    { type: TokenType.string, value: 'simple' },
  ]);
});

test('string with two placeholders', () => {
  expect(lexer('${placeholder1}${placeholder2}')).toEqual([
    { type: TokenType.placeholder, value: 'placeholder1' },
    { type: TokenType.placeholder, value: 'placeholder2' },
  ]);
});

test('string with escaped placeholder', () => {
  expect(lexer('simple\\${placeholder}')).toEqual([{ type: TokenType.string, value: 'simple${placeholder}' }]);
});

test('string with placeholder after escaped dollar', () => {
  expect(lexer('simple\\$${placeholder}')).toEqual([
    { type: TokenType.string, value: 'simple$' },
    { type: TokenType.placeholder, value: 'placeholder' },
  ]);
});

test('string slash before placeholder', () => {
  expect(lexer('simple\\\\${placeholder}')).toEqual([
    { type: TokenType.string, value: 'simple\\' },
    { type: TokenType.placeholder, value: 'placeholder' },
  ]);
});

test('string with placeholder with dot', () => {
  expect(lexer('https://${vars.module.url}')).toEqual([
    { type: TokenType.string, value: 'https://' },
    { type: TokenType.placeholder, value: 'vars.module.url' },
  ]);
});
