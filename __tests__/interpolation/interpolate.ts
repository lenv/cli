import { interpolate as interpolateFactory } from '../../src/utils/interpolation';

const functions = {
  vars: async (key: string) =>
    ({
      'module1.variable1': 'm1v1', //
      'module1.variable2': 'm1v2',
      'module2.variable1': 'm2v1',
    }[key]),
  jobs: async (key: string) =>
    ({
      'job1.artifact1': 'j1a1', //
      'job1.artifact2': 'j1a2',
      'job2.artifact1': 'j2a1',
      'job3.artifactObj': { key: 'value' },
    }[key]),
};

const interpolate = interpolateFactory(functions);

test('string without placeholders', async () => {
  expect(await interpolate('simple')).toEqual('simple');
});

test('string with placeholders in the end', async () => {
  expect(await interpolate('simple-${vars.module1.variable2}')).toEqual('simple-m1v2');
});

test('string with placeholders in the middle', async () => {
  expect(await interpolate('simple-${vars.module2.variable1}-str')).toEqual('simple-m2v1-str');
});

test('string with two placeholders', async () => {
  expect(await interpolate('${jobs.job2.artifact1}-${vars.module2.variable1}')).toEqual('j2a1-m2v1');
});

test('non string placeholder without other strings', async () => {
  expect(await interpolate('${jobs.job3.artifactObj}')).toEqual({ key: 'value' });
});

test('non string placeholder with other strings', async () => {
    expect(await interpolate('obj-${jobs.job3.artifactObj}')).toEqual('obj-[object Object]');
  });
