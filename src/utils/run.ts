import store from '../store';

import jobsStorageState from '../store/jobsStorage';
import jobsArtifactsState from '../store/jobsArtifacts';
import runStagesState from '../store/runStages';
import { v4 as uuidv4 } from 'uuid';

const messageTypes = {
  error: 'error',
  log: 'log',
  info: 'info',
};

const runJob = (job: { name: string; body: (params: any) => Promise<void>; artifacts: any; args: any }) => {
  return new Promise((resolve, reject) => {
    const jobName = job.name;

    const displayMsg = (msg, type) => {
      const value = typeof msg === 'string' ? msg : msg?.toString?.();
      store.dispatch(jobsStorageState.actions.pushOutput({ jobName, output: { type, value } }));
    };

    const log = msg => {
      displayMsg(msg, messageTypes.log);
    };

    const error = msg => {
      displayMsg(msg, messageTypes.error);
    };

    const info = msg => {
      displayMsg(msg, messageTypes.info);
    };

    const textInput = () => {
      return new Promise((resolve, reject) => {
        const id = uuidv4();

        const submit = (value: string) => {
          store.dispatch(
            jobsStorageState.actions.updateOutput({ jobName, id, output: { type: 'textInput', submited: value } }),
          );
          store.dispatch(jobsStorageState.actions.updateStatus({ jobName, status: 'running' }));
          resolve(value);
        };

        store.dispatch(jobsStorageState.actions.updateStatus({ jobName, status: 'waiting' }));
        store.dispatch(jobsStorageState.actions.pushOutput({ jobName, output: { id, type: 'textInput', submit } }));
      });
    };

    const getArtifacts = () => {
      return store.getState().persist.jobsArtifacts[jobName] ?? {};
    };
    const updateArtifacts = artifacts => {
      store.dispatch(jobsArtifactsState.actions.updateArtifacts({ jobName, artifacts }));
    };

    const success = (msg: any) => {
      store.dispatch(jobsStorageState.actions.updateStatus({ jobName, status: 'succeeded' }));
      if (msg) {
        log(msg);
      }

      resolve(null);
    };

    const fail = (msg: any) => {
      store.dispatch(jobsStorageState.actions.updateStatus({ jobName, status: 'failed' }));
      if (msg) {
        error(msg);
      }
      reject(null);
    };

    // run the job
    store.dispatch(jobsStorageState.actions.updateStatus({ jobName, status: 'running' }));
    job.body({
      log,
      error,
      info,
      success,
      fail,
      updateArtifacts,
      getArtifacts,
      artifacts: job.artifacts,
      args: job.args,
      textInput,
    });
  });
};

export const run = async () => {
  const state = store.getState();
  store.dispatch(jobsStorageState.actions.clearOutput());
  store.dispatch(jobsStorageState.actions.clearStatuses());

  const stages = runStagesState.selectors.activeRunStages(state);

  try {
    for (let i = 0; i < stages.length; i++) {
      const stage = stages[i];
      const { jobs } = stage;
      await Promise.all(jobs.map(runJob));
    }
  } catch (err) {}
};
