import * as fs from 'fs';
import * as path from 'path';
import { mergeDeepRight } from 'ramda';
import * as YAML from 'yaml';

import store from '../store';
import enabledModulesState from '../store/enabledModules';
import modulesState from '../store/modules';
import { interpolate as interpolateFactory } from './interpolation';

const dockerComposePath = path.join(process.cwd(), '/docker-compose.yaml');

export const buildJob = {
  name: 'docker-compose',
  stage: 'build',
  body: ({ success, fail }) => build().then(success).catch(fail),
};

export const build = async () => {
  const state = store.getState();

  const enabledModules = enabledModulesState.selectors.enabledModules(state);
  const dependencies = enabledModulesState.selectors.dependencies(state).modules;

  const allModules = [...enabledModules, ...dependencies];
  const mergedConfigs = allModules.map(m => {
    const M = modulesState.selectors.moduleByName(m)(state);
    const activeConfig = modulesState.selectors.activeConfig(m)(state);
    return { ...M, config: activeConfig };
  });

  const resolvedVariables = await resolveVariables(mergedConfigs);
  const dockerYaml = YAML.stringify(
    JSON.parse(JSON.stringify(resolvedVariables.docker)), // to prevent aliases in yaml
    {},
  );

  fs.writeFileSync(dockerComposePath, dockerYaml);
};

const resolveVariables = async modules => {
  const CustomFunctions = store.getState().functions.reduce(
    (acc, f) => ({
      ...acc,
      [f.name]: f.body,
    }),
    {},
  );

  const CustomInterpolationFunctions = store.getState().functions.reduce(
    (acc, f) => ({
      ...acc,
      [f.interpolationPrefix]: f.body,
    }),
    {},
  );

  const variables = {};
  const variablesArr = [];
  const rejectIfPendingAfter = (timeout: number, reject: (reason: any) => void) => {
    setTimeout(() => {
      const pendingVariables = variablesArr.filter(p => !p.fulfilled);
      if (pendingVariables.length > 0) {
        reject(new Error(`Undefined variables: ${pendingVariables.map(p => p.location).join(', ')}`));
      }
    }, 1000);
  };

  const P = <T>() => {
    let resolve: (v: T) => void;
    const value: any = new Promise<T>((res, rej) => (resolve = res));
    value.then(r => {
      value.fulfilled = true;
    });
    value.resolve = (value: any) => {
      if (value.then) {
        value.then(res => resolve(res));
      } else {
        resolve(value);
      }
    };

    return value;
  };

  const getModuleVariables = moduleName => {
    if (!variables[moduleName]) variables[moduleName] = {};
    return variables[moduleName];
  };

  const getVariable = (moduleName, variableName) => {
    const moduleVariables = getModuleVariables(moduleName);
    if (!moduleVariables[variableName]) {
      moduleVariables[variableName] = P();
      moduleVariables[variableName].location = `${moduleName}.${variableName}`;
      variablesArr.push(moduleVariables[variableName]);
    }
    return moduleVariables[variableName];
  };

  const getVar = (varName: string) => {
    const [moduleName, ...rest] = varName.split('.');
    const variableName = rest.join('.');
    return getVariable(moduleName, variableName);
  };

  const getArtifact = (artifactName: string) => {
    const [jobName, ...rest] = artifactName.split('.');
    const variableName = rest.join('.');
    return store.getState().persist.jobsArtifacts?.[jobName]?.[variableName];
  };

  const interpolate = interpolateFactory({
    vars: getVar,
    jobs: getArtifact,
    ...CustomInterpolationFunctions,
  });

  const resolveNested = async obj => {
    if (Array.isArray(obj)) {
      return await Promise.all(obj.map(resolveNested));
    } else if (typeof obj === 'function') {
      return await obj({ getVar, getArtifact, ...CustomFunctions });
    } else if (typeof obj === 'string') {
      return interpolate(obj);
    } else if (typeof obj === 'object') {
      const res = {};
      for (const property in obj) {
        res[property] = await resolveNested(obj[property]);
      }
      return res;
    } else {
      return obj;
    }
  };

  modules.forEach(m => {
    if (m?.config?.output) {
      Object.entries(m.config.output).forEach(([variableName, value]) => {
        const resVar = getVariable(m.name, variableName);
        if (typeof value === 'function') {
          resVar.resolve(value({ getVar, getArtifact, ...CustomFunctions }));
        } else if (typeof value === 'string') {
          resVar.resolve(interpolate(value));
        } else {
          resVar.resolve(value);
        }
      });
    }
  });

  await new Promise((resolve, reject) => {
    Promise.all(variablesArr).then(resolve);
    rejectIfPendingAfter(1000, reject);
  });

  const docker = (
    await new Promise<any[]>((resolve, reject) => {
      Promise.all(
        modules.map(async m => {
          if (m?.config?.docker) {
            return resolveNested(m.config.docker);
          }
        }),
      )
        .then(resolve)
        .catch(reject);
      rejectIfPendingAfter(1000, reject);
    })
  )
    .filter(Boolean)
    .reduce(mergeDeepRight, {});

  return { variables, docker };
};
