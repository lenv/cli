const { exec } = require('child_process');

export const runDockerJob = {
  name: 'docker-compose up',
  stage: 'run',
  body: ({ success, fail, log, error }) => {
    try {
      const child = exec('docker-compose up -d --remove-orphans', (err, stdout, stderr) => {
        if (err) {
          return fail(`error: ${err.message}`);
        }
        if (stderr) {
          // fail(`${stderr}`);
        }
        success();
      });

      child.stdout.on('data', function (data) {
        log(data.toString());
      });
      child.stderr.on('data', function (data) {
        error(data.toString());
      });
    } catch (e) {
      fail(e);
    }
  },
};
