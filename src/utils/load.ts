import * as glob from 'glob';
import * as path from 'path';
import * as fs from 'fs';
import YAML from 'yaml';
import { buildJob } from './build';
import { runDockerJob } from './runDocker';

const loadStrategies = {
  '.js': (filePath: string) => require(filePath), //
  '.yaml': (filePath: string) => YAML.parse(fs.readFileSync(filePath, 'utf8')),
};

const loadFile = (modulePath: string) => {
  try {
    const extension = path.extname(modulePath);
    return loadStrategies[extension](modulePath);
  } catch (err) {
    throw new Error(`Can't load file ${modulePath}. ${err.toString()}`);
  }
};

const processRelativePath = (filePath: string) => {
  return path.join(process.cwd(), filePath);
};

export const loadModules = () => {
  return [
    ...glob.sync(processRelativePath('/modules/**/*.module.js')), //
    ...glob.sync(processRelativePath('/modules/**/*.module.yaml')),
  ].map(modulePath => ({
    ...loadFile(modulePath),
    configs: [
      ...glob.sync(path.join(modulePath, '../configs/*.js')), //
      ...glob.sync(path.join(modulePath, '../configs/*.yaml')), //
    ].map(loadFile),
  }));
};

export const loadJobs = () => {
  return [
    ...glob.sync(processRelativePath('/jobs/**/*.job.js')).map(loadFile), //
    buildJob,
    runDockerJob,
  ];
};

export const loadFunctions = () => {
  return glob.sync(processRelativePath('/functions/**/*.function.js')).map(loadFile);
};

export const loadConfig = () => {
  return glob.sync(processRelativePath('/lenv.config.js')).map(loadFile)?.[0];
};
