export enum TokenType {
  string = 'string',
  placeholder = 'placeholder',
}

export type Token = {
  type: TokenType;
  value: string;
};

export const lexer = (str: string) => {
  const result: Token[] = [];
  let currentIndex = 0;

  const currentChar = () => {
    return str[currentIndex];
  };
  const nextChar = () => {
    return str[currentIndex + 1];
  };
  const pushToken = (type: TokenType, value: string) => {
    result.push({ type, value });
  };

  const moveCurrentIndex = () => {
    currentIndex += 1;
  };

  const isEnd = () => {
    return currentIndex === str.length;
  };

  const isStartOfPlaceholder = () => {
    return currentChar() === '$' && nextChar() === '{';
  };

  const readString = () => {
    let buffer = '';
    while (!isEnd() && !isStartOfPlaceholder()) {
      if (currentChar() === '\\') {
        // escape characters
        buffer += nextChar();
        moveCurrentIndex();
        moveCurrentIndex();
      } else {
        buffer += currentChar();
        moveCurrentIndex();
      }
    }
    if (buffer.length > 0) {
      pushToken(TokenType.string, buffer);
    }
  };

  const readPlaceholder = () => {
    let buffer = '';

    moveCurrentIndex();
    moveCurrentIndex();

    while (!isEnd() && currentChar() !== '}') {
      buffer += currentChar();
      moveCurrentIndex();
    }
    moveCurrentIndex();

    if (buffer.length > 0) {
      pushToken(TokenType.placeholder, buffer);
    }
  };

  while (!isEnd()) {
    if (isStartOfPlaceholder()) {
      readPlaceholder();
    } else {
      readString();
    }
  }

  return result;
};
