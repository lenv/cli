import { TokenType, lexer, Token } from './lexer';

export const interpolate = (functions: Record<string, (str: string) => Promise<any>>) => {
  const mapToken: Record<TokenType, (v: string) => string | Promise<string>> = {
    [TokenType.string]: v => v,
    [TokenType.placeholder]: v => {
      const [fName, ...args] = v.split('.');
      return functions[fName]?.(args.join('.'));
    },
  };

  return async (str: string) => {
    const resolved = await Promise.all(lexer(str).map(t => mapToken[t.type](t.value)));
    
    if (resolved.length === 1) return resolved[0];
    return resolved.join('');
  };
};
