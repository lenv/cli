import React, { useEffect } from 'react';
import { Box, Text } from 'ink';
import figures from 'figures';

type NotificationProps = {
  message: string;
  timeout: number;
  onHide?: () => void;
};
export const Notification = ({ message, timeout, onHide }: NotificationProps) => {
  useEffect(() => {
    const timer = setTimeout(() => onHide?.(), timeout);
    return () => {
      clearTimeout(timer);
    };
  });

  return (
    <Box marginLeft={2} marginTop={1}>
      <Text color="magenta">
        {figures.info} {message}
      </Text>
    </Box>
  );
};

export default Notification;
