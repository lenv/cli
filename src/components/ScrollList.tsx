import React from 'react';
import { Box, Text } from 'ink';
import figures from 'figures';

type ScrollListProps<T> = {
  items: T[];
  selectedIndex: number;
  renderItem: (props: { item: T; selected: boolean }) => JSX.Element;
  maxHeight?: number;
  ellipsisTop?: (props: { hasHiddenItems?: boolean }) => JSX.Element;
  ellipsisBottom?: (props: { hasHiddenItems?: boolean }) => JSX.Element;
};

const ellipsis = ({ hasHiddenItems }) => (hasHiddenItems ? <Text>{figures.ellipsis}</Text> : <Box />);

const ScrollList = <T, >(props: ScrollListProps<T>) => {
  const maxTake = props.maxHeight ?? 20;
  const maxSkip = Math.max(0, props.items.length - maxTake);

  const skip = Math.min(Math.max(0, props.selectedIndex - maxTake / 2), maxSkip);

  const hiddenTop = skip > 0;
  const hiddenBottom = skip < maxSkip;

  const take = maxTake - (hiddenTop ? 1 : 0) - (hiddenBottom ? 1 : 0);
  const skipWithEllipsis = skip > 0 ? skip + 1 : 0;

  const itemsToRender = props.items.slice(skipWithEllipsis, skipWithEllipsis + take);

  const EllipsisTop = props.ellipsisTop ?? ellipsis;
  const EllipsisBottom = props.ellipsisBottom ?? ellipsis;

  const ItemComponent = props.renderItem;

  return (
    <Box flexDirection='column'>
      {(hiddenTop || hiddenBottom) && <EllipsisTop hasHiddenItems={hiddenTop} />}
      {itemsToRender.map((item, i) => {
        return (
          <Box key={i}>
            <ItemComponent item={item} selected={i === props.selectedIndex - skipWithEllipsis} />
          </Box>
        );
      })}
      {(hiddenTop || hiddenBottom) && <EllipsisBottom hasHiddenItems={hiddenBottom} />}
    </Box>
  );
};

export default ScrollList;
