import React, { useEffect, useState } from 'react';
import { Box } from 'ink';
import useStdoutDimensions from 'ink-use-stdout-dimensions';

export default function Fullscreen({ children }: { children?: JSX.Element | JSX.Element[] }) {
  const [columns, rows] = useStdoutDimensions();
  return (
    <Box flexDirection={'column'} width={columns} height={rows}>
      {children}
    </Box>
  );
}
