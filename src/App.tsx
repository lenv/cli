import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Spinner from 'ink-spinner';
import { Box, Text } from 'ink';
import useHotReload from './hooks/useHotReload';

import store, { persistor } from './store';
import Router from './Router';

const Loader = () => {
  return (
    <Box>
      <Box width={2}>
        <Spinner />
      </Box>
      <Text>Loading state...</Text>
    </Box>
  );
};

const App = () => {
  useHotReload();

  return <Router />;
};

const ProvidersWrapper = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={<Loader />} persistor={persistor}>
        <App />
      </PersistGate>
    </Provider>
  );
};

export default ProvidersWrapper;
