import { useDispatch, useSelector } from 'react-redux';
import routerState from '../store/router';

export const useNavigation = () => {
  const dispatch = useDispatch();
  const path = useSelector(routerState.selectors.getPath);
  const push = (route: string, params?: Record<string, unknown>) => {
    dispatch(routerState.actions.push({ route, params }));
  };

  const pop = () => {
    dispatch(routerState.actions.pop());
  };

  return { push, pop, path };
};

export default useNavigation;
