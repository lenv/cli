import { useState } from 'react';
import { useInput } from 'ink';

export const useSelectedIndex = ({
  min = 0,
  max = Infinity,
  interactive = true,
  direction = 'vertical' as 'vertical' | 'horizontal',
}) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  useInput((input, key) => {
    if (interactive) {
      if ((key.upArrow && direction === 'vertical') || (key.leftArrow && direction === 'horizontal')) {
        setSelectedIndex(i => Math.max(min, i - 1));
      }

      if ((key.downArrow && direction === 'vertical') || (key.rightArrow && direction === 'horizontal')) {
        setSelectedIndex(i => Math.min(max, i + 1));
      }
    }
  });

  return selectedIndex;
};

export default useSelectedIndex;
