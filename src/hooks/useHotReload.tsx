import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import modulesState from '../store/modules';
import jobsState from '../store/jobs';
import configState from '../store/config';
import functionsState from '../store/functions';

import { loadModules, loadJobs, loadConfig, loadFunctions } from '../utils/load';

import chokidar from 'chokidar';
import * as path from 'path';

export const useHotReload = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    const watcher = chokidar.watch(
      [
        path.join(process.cwd(), '/modules/**/*.js'), //
        path.join(process.cwd(), '/modules/**/*.yaml'), //
        path.join(process.cwd(), '/jobs/**/*.js'),
        path.join(process.cwd(), '/functions/**/*.js'),
        path.join(process.cwd(), '/lenv.config.js'),
      ],
      { ignoreInitial: true },
    );
    const handler = (filePath?: string) => {
      filePath && delete require.cache[require.resolve(filePath)];
      dispatch(modulesState.actions.setModules(loadModules()));
      dispatch(jobsState.actions.setJobs(loadJobs()));
      dispatch(functionsState.actions.setFunctions(loadFunctions()));

      const config = loadConfig();
      config && dispatch(configState.actions.setConfig(config));
    };
    handler();

    watcher.on('add', handler);
    watcher.on('change', handler);

    return () => {
      watcher.close();
    };
  }, []);
};

export default useHotReload;
