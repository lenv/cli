import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Text, Box, useInput } from 'ink';
import runStagesState from '../../store/runStages';
import jobsStorageState, { Status } from '../../store/jobsStorage';
import useNavigation from '../../hooks/useNavigation';
import Spinner from 'ink-spinner';
import figures from 'figures';
import routerState from '../../store/router';
import ScrollList from '../../components/ScrollList';

const StatusView: Record<Status, React.ReactNode> = {
  paused: figures.squareSmallFilled,
  running: <Spinner />,
  succeeded: figures.tick,
  failed: figures.cross,
  waiting: figures.pointer,
};

const Colors: Record<Status, string> = {
  paused: 'default',
  running: 'blue',
  succeeded: 'green',
  failed: 'red',
  waiting: 'yellow',
};


const Job = (props: { name: string; selected?: boolean }) => {
  const status = useSelector(jobsStorageState.selectors.jobStatus(props.name));
  return (
    <Box width={props.name.length + 10} justifyContent='center'>
      <Box borderStyle={'round'} borderColor={props.selected ? 'green' : 'black'} paddingX={2}>
        <Text color={Colors[status]}>
          {StatusView[status]} {props.name}
        </Text>
      </Box>
    </Box>
  );
};

const Stage = (props: { name: string; jobs: { name: string }[]; selectedJob: number }) => {
  return (
    <Box flexDirection='column'>
      <Text color='yellow'>{props.name}:</Text>
      <Box flexDirection='row'>
        {props.jobs.map((job, i) => (
          <Job key={i} name={job.name} selected={props.selectedJob === i} />
        ))}
      </Box>
    </Box>
  );
};

export const Run = () => {
  const navigation = useNavigation();
  const path = useSelector(routerState.selectors.getPath);
  const navigator = useNavigation();
  const stages = useSelector(runStagesState.selectors.activeRunStages);

  const [selectedRow, setSelectedRow] = useState(0);
  const [selectedColumn, setSelectedColumn] = useState(0);
  const [jobSelected, setJobSelected] = useState(false);

  useInput(
    (input, key) => {
      if (key.rightArrow) {
        setSelectedColumn(c => Math.min(stages[selectedRow]?.jobs.length - 1, c + 1));
      }
      if (key.leftArrow) {
        setSelectedColumn(c => Math.max(0, c - 1));
      }
      if (key.upArrow) {
        setSelectedRow(c => Math.max(0, c - 1));
      }
      if (key.downArrow) {
        setSelectedRow(c => Math.min(stages.length - 1, c + 1));
      }

      if (key.return) {
        navigator.push('ContextScreen', { jobName: stages?.[selectedRow]?.jobs?.[selectedColumn]?.name });


      }
    },
    { isActive: navigator.path === 'Home' },
  );
  useEffect(() => {
    setSelectedColumn(c => Math.min(stages[selectedRow]?.jobs.length - 1, c));
  }, [selectedRow]);

  return <>
    <Box flexDirection='column'>
      {stages.map((stage, i) => (
        <Stage key={i} name={stage.name} jobs={stage.jobs} selectedJob={selectedRow === i ? selectedColumn : -1} />
      ))}
    </Box>


  </>;
};

export default Run;
;
