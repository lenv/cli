import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { run } from '../../utils/run';
import { Box, Text, useInput, useApp } from 'ink';
import FullScreen from '../../components/Fullscreen';

import SelectedModulesScreen from '../SelectedModulesScreen';
import AddModulesScreen from '../AddModulesScreen';
import RunScreen from '../RunScreen';
import { Version } from './Version';
import EnabledModulesState from '../../store/enabledModules';
import routerState from '../../store/router';

type TabsProps = {
  items: string[];
  selectedIndex: number;
};

const Tabs = (props: TabsProps) => {
  return (
    <Box alignItems='center'>
      {props.items.map((item, i) => {
        const selected = i === props.selectedIndex;
        return (
          <Box key={i} width={item.length + 6} justifyContent='center'>
            <Box borderStyle={selected ? 'round' : undefined} borderColor='green' paddingX={2}>
              <Text color={selected ? 'green' : undefined}>{item}</Text>
            </Box>
          </Box>
        );
      })}
      {props.items.length > 1 && (
        <Box marginLeft={3}>
          <Text color='blue'>(TAB to switch)</Text>
        </Box>
      )}
    </Box>
  );
};

export const HomeScreen = props => {

  const enabledModules = useSelector(EnabledModulesState.selectors.enabledModules);
  const { exit } = useApp();
  const items = [
    {
      title: 'Add modules',
      component: AddModulesScreen,
    },
    ...(enabledModules.length > 0
      ? [
        {
          title: 'Choose configurations',
          component: SelectedModulesScreen,
        },
      ]
      : []),
    {
      title: 'Run',
      component: RunScreen,
    },
  ];
  const [selectedIndex, setSelectedIndex] = useState(0);

  useInput((input, key) => {
    if (key.tab) {
      setSelectedIndex(selectedIndex => (selectedIndex + 1) % items.length);
    }
    if (input === 'b' || input === 'B') {
      run();
    }
    if (input === 'q' || input === 'Q') {
      exit();
    }
  });

  const Component = items[selectedIndex].component;

  return (
    <FullScreen>
      <Box height={25} flexDirection='column'>
        <><Tabs items={items.map(item => item.title)} selectedIndex={selectedIndex} />
          <Box paddingX={3} flexDirection='column'>
            <Component {...props} />
          </Box></>
      </Box>
      <Box width='100%' flexDirection='row' justifyContent='space-between'>
        <Text inverse>Arrow keys - navigation. B - build. Q - exit.</Text>
        <Version />
      </Box>

    </FullScreen>
  );
};

export default HomeScreen;
