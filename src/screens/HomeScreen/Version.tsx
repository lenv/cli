import React, { useEffect, useState } from 'react';
import { Text } from 'ink';
import latestVersion from 'latest-version';

const { name, version } = require('../../../package.json');

export const Version = () => {
  const [updateAvailable, setUpdateAvailable] = useState(false);
  useEffect(() => {
    latestVersion(name)
      .then(latestVersion => {
        if (latestVersion !== version) {
          setUpdateAvailable(true);
        }
      })
      .catch(err => {
        // Do nothing
      });
  });

  return (
    <Text>
      v{version}
      <Text color="yellow">{updateAvailable && ' (update available)'}</Text>
    </Text>
  );
};
