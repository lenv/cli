import React from 'react';
import { Box, Text, useInput, } from 'ink';
import { useSelector, useDispatch } from 'react-redux';
import figures from 'figures';
import { useSelectedIndex } from '../hooks/useSelectedIndex';
import { useNavigation } from '../hooks/useNavigation';
import ScrollList from '../components/ScrollList';
import enabledModulesState from '../store/enabledModules';
import modulesState from '../store/modules';

const renderItem = ({ item, selected }) => {
  const enabled = useSelector(enabledModulesState.selectors.isEnabled(item));
  return (
    <>
      <Box width={2}>
        {enabled ? <Text color="green">{figures.radioOn}</Text> : <Text color="green">{figures.radioOff}</Text>}
      </Box>
      <Text color={selected ? 'green' : ''}>{item}</Text>
    </>
  );
};

const AddModules = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const modules = useSelector(modulesState.selectors.modulesNames);
  const selectedIndex = useSelectedIndex({ min: 0, max: modules.length - 1 });

  useInput((input, key) => {
    if (input === ' ') {
      dispatch(enabledModulesState.actions.toggle(modules[selectedIndex]));
    }
    if (key.escape) {
      navigation.pop();
    }
  });

  return (
    <>
      {/* <Text color="blue">Space - toggle module. Arrows keys - navigation. Escape - return back.</Text> */}
      {/* <Box height={1} /> */}
      <Text color="yellow">Available modules ({modules.length}):</Text>
      <ScrollList items={modules} selectedIndex={selectedIndex} renderItem={renderItem} maxHeight={20} />
    </>
  );
};

export default AddModules;
