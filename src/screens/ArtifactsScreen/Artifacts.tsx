import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Text, Box, useInput } from 'ink';
import jobsStorageState, { Status } from '../../store/jobsStorage';

import useNavigation from '../../hooks/useNavigation';
import Spinner from 'ink-spinner';
import figures from 'figures';

import store from '../../store';
import IncTextInput from 'ink-text-input';
import jobsArtifactsState from '../../store/jobsArtifacts';
import Fullscreen from '../../components/Fullscreen';

const StatusView: Record<Status, React.ReactNode> = {
  paused: figures.squareSmallFilled,
  running: <Spinner />,
  succeeded: figures.tick,
  failed: figures.cross,
  waiting: figures.pointer,
};

const Colors: Record<Status, string> = {
  paused: 'default',
  running: 'blue',
  succeeded: 'green',
  failed: 'red',
  waiting: 'yellow',
};

const Job = (props: { name: string; selected?: boolean }) => {
  const status = useSelector(jobsStorageState.selectors.jobStatus(props.name));
  return (
    <Box width={props.name.length + 10} justifyContent="center">
      <Box borderStyle={'round'} borderColor={props.selected ? 'green' : 'black'} paddingX={2}>
        <Text color={Colors[status]}>
          {StatusView[status]} {props.name}
        </Text>
      </Box>
    </Box>
  );
};

export const Artifacts = props => {
  const { jobName } = props.navigation?.params;
  const navigator = useNavigation();

  const [selectedRow, setSelectedRow] = useState(0);

  const [selectedArtifact, setSelectedArtifact] = useState(null);

  const [input, setInput] = useState('');

  const artifactsSelector = useSelector(jobsArtifactsState.selectors.jobsArtifacts);
  const artifacts = artifactsSelector[jobName] ?? {};

  useInput((input, key) => {
    if (key.upArrow) {
      setSelectedRow(c => Math.max(0, c - 1));
    }
    if (key.downArrow) {
      setSelectedRow(c => Math.min(Object.keys(artifacts).length - 1, c + 1));
    }

    if (key.return && !selectedArtifact && Object.keys(artifacts).length > 0) {
      const prop = Object.keys(artifacts)[selectedRow];
      setSelectedArtifact({ value: artifacts[prop], label: prop });
    }

    if (key.escape) {
      if (!selectedArtifact) navigator.pop();
      else setSelectedArtifact(null);
    }
  });

  if (selectedArtifact) {
    return (
      <Fullscreen>
        <Text inverse>{figures.arrowLeft} escape</Text>
        <Text>{selectedArtifact.label}</Text>
        <Text>Current Value : {selectedArtifact.value}</Text>
        <Box flexDirection="row">
          <Text>New Value {figures.pointer} </Text>
          <IncTextInput
            value={input}
            onChange={setInput}
            onSubmit={v => {
              store.dispatch(
                jobsArtifactsState.actions.updateArtifacts({
                  jobName,
                  artifacts: { [selectedArtifact.label]: v },
                }),
              );
              setInput('');
              setSelectedArtifact(null);
            }}
          />
        </Box>
      </Fullscreen>
    );
  }
  return (
    <Fullscreen>
      <Text inverse>{figures.arrowLeft} escape</Text>
      <Text>{jobName}</Text>
      <Text>Artifacts</Text>
      <Box flexDirection="column">
        {Object.keys(artifacts).map((prop, i) => (
          <Job key={i} name={prop} selected={i === selectedRow} />
        ))}
      </Box>
    </Fullscreen>
  );
};

export default Artifacts;
