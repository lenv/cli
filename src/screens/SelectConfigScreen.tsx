import React from 'react';
import { Box, Text, useInput } from 'ink';
import { useSelector, useDispatch } from 'react-redux';
import figures from 'figures';
import { useSelectedIndex } from '../hooks/useSelectedIndex';
import { useNavigation } from '../hooks/useNavigation';
import ScrollList from '../components/ScrollList';
import modulesState from '../store/modules';
import selectedConfigsState from '../store/selectedConfigs';

const SelectConfigScreen = props => {
  const { moduleName } = props.navigation?.params;
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const module = useSelector(modulesState.selectors.moduleByName(moduleName));
  const activeConfig = useSelector(modulesState.selectors.activeConfig(moduleName));
  const selectedIndex = useSelectedIndex({
    min: 0,
    max: module.configs.length - 1,
  });

  const selectedConfig = module.configs[selectedIndex];

  useInput((input, key) => {
    // if (input === ' ') {
    // 	dispatch(enabledModulesState.actions.toggle(modules[selectedIndex]));
    // }
    if (key.return) {
      dispatch(
        selectedConfigsState.actions.set({
          moduleName,
          configName: selectedConfig.name,
        }),
      );
      navigation.pop();
    }
    if (key.escape) {
      navigation.pop();
    }
  });

  const renderItem = ({ item, selected }) => {
    const color = selected ? 'green' : '';
    return (
      <>
        <Box width={2} height={1}>
          {activeConfig.name === item.name && <Text color={color}>{figures.tick}</Text>}
        </Box>
        <Box width={40}>
          <Text color={color}>{item.name}</Text>
        </Box>
        <Box width={80}>
          <Text color={color}>{item.description}</Text>
        </Box>
      </>
    );
  };

  return (
    <>
      {/* <Text color="blue">
        Arrows keys - navigation. Enter - select. Escape - return back.
      </Text> */}
      <Box flexDirection="column" paddingX={3}>
        <Box height={1} />
        <Text color="yellow">
          Available configurations for module {moduleName} ({module.configs.length}):
        </Text>
        <ScrollList items={module.configs} selectedIndex={selectedIndex} renderItem={renderItem} maxHeight={20} />
      </Box>
    </>
  );
};

export default SelectConfigScreen;
