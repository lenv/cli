import React, { useState } from 'react';
import { Box, Text, useInput, useApp } from 'ink';
import figures from 'figures';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '../hooks/useNavigation';
import { useSelectedIndex } from '../hooks/useSelectedIndex';
import ScrollList from '../components/ScrollList';
import Notification from '../components/Notification';
import enabledModulesState from '../store/enabledModules';
import modulesState from '../store/modules';
import selectedConfigsState from '../store/selectedConfigs';

const EditModuleMenu = ({ moduleName, removable }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const items = [
    {
      title: 'Select config',
      callback: () => {
        navigation.push('SelectConfigScreen', { moduleName });
      },
    },
    removable && {
      title: 'Remove',
      callback: () => {
        dispatch(enabledModulesState.actions.remove(moduleName));
      },
    },
  ].filter(Boolean);

  const selectedIndex = useSelectedIndex({
    min: 0,
    max: items.length - 1,
    direction: 'horizontal',
  });

  useInput((input, key) => {
    if (key.return) {
      items[selectedIndex]?.callback();
      // setEditMode(true);
    }
  });

  return (
    <Box paddingLeft={4}>
      {items.map((item, i) => {
        return (
          <Box paddingX={1} key={i}>
            <Text color={i === selectedIndex ? 'green' : ''}>{item.title}</Text>
          </Box>
        );
      })}
    </Box>
  );
};

const renderItem = (options: { editMode: boolean; removable: boolean }) => ({ item, selected }) => {
  const { configs = [] } = useSelector(modulesState.selectors.moduleByName(item)) ?? {};
  const activeConfig = useSelector(modulesState.selectors.activeConfig(item));
  const showArrows = selected && configs.length > 1;

  return (
    <>
      <Box width={2}>{selected && <Text color='green'>{figures.pointerSmall}</Text>}</Box>
      <Box width={40}>
        <Text color={selected && !options.editMode ? 'green' : ''}>{item}</Text>
      </Box>
      <Box width={40}>
        <Box width={3} height={1} justifyContent='center'>
          {showArrows && <Text color='green'>{figures.arrowLeft}</Text>}
        </Box>
        <Text color={selected && !options.editMode ? 'green' : ''}>{activeConfig?.name}</Text>
        <Box width={3} height={1} justifyContent='center'>
          {showArrows && <Text color='green'>{figures.arrowRight}</Text>}
        </Box>
      </Box>
      {options.editMode && selected && <EditModuleMenu moduleName={item} removable={options.removable} />}
    </>
  );
};

const Ellipsis = ({ hasHiddenItems }) => {
  return (
    <Box>
      <Box width={2}></Box>
      {hasHiddenItems && <Text>{figures.ellipsis}</Text>}
    </Box>
  );
};

const Home = props => {
  const isForeground = props.navigation?.foreground;
  const enabledModules = useSelector(enabledModulesState.selectors.enabledModules);
  const dependencies = useSelector(enabledModulesState.selectors.dependencies).modules;

  const { exit } = useApp();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [editMode, setEditMode] = useState(false);
  const [notification, setNotification] = useState(null as string);
  const selectedIndex = useSelectedIndex({
    min: 0,
    max: enabledModules.length + dependencies.length - 1,
    interactive: isForeground && !editMode,
  });

  const selectedModuleName = [...enabledModules, ...dependencies][selectedIndex];
  const selectedModule = useSelector(modulesState.selectors.moduleByName(selectedModuleName));
  const activeConfig = useSelector(modulesState.selectors.activeConfig(selectedModuleName));

  useInput((input, key) => {
    if (isForeground) {

      if (key.escape) {
        setEditMode(false);
      }

      if (key.return) {
        navigation.push('SelectConfigScreen', {
          moduleName: [...enabledModules, ...dependencies][selectedIndex],
        });
        // if (!editMode) {
        //   setEditMode(true);
        // } else {
        //   setEditMode(false);
        // }
      }
      if (key.leftArrow) {
        const activeConfigIndex = selectedModule.configs.findIndex(C => C.name === activeConfig.name);

        dispatch(
          selectedConfigsState.actions.set({
            moduleName: selectedModuleName,
            configName:
            selectedModule.configs[
            (selectedModule.configs.length + (activeConfigIndex - 1)) % selectedModule.configs.length
              ].name,
          }),
        );
      }
      if (key.rightArrow) {
        const activeConfigIndex = selectedModule.configs.findIndex(C => C.name === activeConfig.name);

        dispatch(
          selectedConfigsState.actions.set({
            moduleName: selectedModuleName,
            configName: selectedModule.configs[(activeConfigIndex + 1) % selectedModule.configs.length].name,
          }),
        );
      }
    }
  });

  return (
    <Box flexDirection='column'>
      {/* <Text color="blue">
        A - add modules. Arrows keys - navigation. Enter - action. B - build.
      </Text> */}
      {/* <Box height={1} /> */}
      <Text color='yellow'>Selected modules ({enabledModules.length}):</Text>
      <ScrollList
        items={enabledModules}
        renderItem={renderItem({ editMode, removable: true })}
        selectedIndex={selectedIndex}
        maxHeight={10}
        ellipsisTop={Ellipsis}
        ellipsisBottom={Ellipsis}
      />
      <Box height={1} />
      <Text color='yellow'>Dependencies ({dependencies.length}):</Text>
      <ScrollList
        items={dependencies}
        renderItem={renderItem({ editMode, removable: false })}
        selectedIndex={selectedIndex - enabledModules.length}
        maxHeight={10}
        ellipsisTop={Ellipsis}
        ellipsisBottom={Ellipsis}
      />
      {notification && <Notification message={notification} timeout={3000} onHide={() => setNotification(null)} />}
    </Box>
  );
};

export default Home;
