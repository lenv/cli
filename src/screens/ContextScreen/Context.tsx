import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Text, Box, useInput } from 'ink';
import jobsStorageState, { Status } from '../../store/jobsStorage';

import useNavigation from '../../hooks/useNavigation';
import Spinner from 'ink-spinner';
import figures from 'figures';

import store from '../../store';
import jobsArtifactsState from '../../store/jobsArtifacts';
import Fullscreen from '../../components/Fullscreen';
import routerState from '../../store/router';

const StatusView: Record<Status, React.ReactNode> = {
  paused: figures.squareSmallFilled,
  running: <Spinner />,
  succeeded: figures.tick,
  failed: figures.cross,
  waiting: figures.pointer,
};

const Colors: Record<Status, string> = {
  paused: 'default',
  running: 'blue',
  succeeded: 'green',
  failed: 'red',
  waiting: 'yellow',
};


const Job = (props: { name: string; selected?: boolean }) => {
  const status = useSelector(jobsStorageState.selectors.jobStatus(props.name));
  return (
    <Box width={props.name.length + 10} justifyContent='center'>
      <Box borderStyle={'round'} borderColor={props.selected ? 'green' : 'black'} paddingX={2}>
        <Text color={Colors[status]}>
          {StatusView[status]} {props.name}
        </Text>
      </Box>
    </Box>
  );
};


export const Context = (props) => {
  const { jobName } = props.navigation?.params;
  const navigator = useNavigation();


  const [selectedRow, setSelectedRow] = useState(0);


  const options = [{
    label: 'View Logs',
    screen: 'JobScreen',
    params: { jobName },
  },
    {
      label: 'Show Artifacts',
      screen: 'ArtifactsScreen',
      params: { jobName },

    },
    {
      label: 'Delete Artifacts',
      func: () => {
        store.dispatch(jobsArtifactsState.actions.deleteArtifacts({ jobName }));
      },

    }];

  useInput(
    (input, key) => {

      if (key.upArrow) {
        setSelectedRow(c => Math.max(0, c - 1));
      }
      if (key.downArrow) {
        setSelectedRow(c => Math.min(options.length - 1, c + 1));
      }

      if (key.return) {
        if (options[selectedRow].screen) {
          navigator.push(options[selectedRow].screen, { ...options[selectedRow].params });

        } else if (options[selectedRow].func) {
          options[selectedRow].func();
        }


      }

      if (key.escape) {
        navigator.pop();


      }

    }, { isActive: navigator.path === 'Home.ContextScreen' },
  );

  return (
    <Fullscreen>
      <Box height={25} flexDirection={'column'}>
        <Text inverse>{figures.arrowLeft} escape</Text>
        <Text>{jobName}</Text>
        <Box flexDirection='column'>
          {options.map((job, i) => (
            <Job key={i} name={job.label} selected={i === selectedRow} />
          ))}
        </Box>
      </Box>
    </Fullscreen>

  );
};

export default Context;
;
