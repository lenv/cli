import React, { useState } from 'react';
import { Box, Text, useInput } from 'ink';
import { useSelector } from 'react-redux';
import { useNavigation } from '../../hooks/useNavigation';
import jobsStorageState from '../..//store/jobsStorage';
import figures from 'figures';
import IncTextInput from 'ink-text-input';
import Fullscreen from '../../components/Fullscreen';


const JobScreen = props => {
  const { jobName } = props.navigation?.params;
  const navigation = useNavigation();
  const jobOutput = useSelector(jobsStorageState.selectors.jobOutput(jobName));


  useInput((input, key) => {
    if (key.escape) {
      navigation.pop();
    }
  });

  return (
    <Fullscreen>
      <Text inverse>{figures.arrowLeft} escape</Text>
      <Box paddingTop={1} paddingLeft={2} flexDirection='column'>
        <Text color='yellow'>{jobName}:</Text>
        {jobOutput.map((item, i) => {
          return React.createElement(OutputViews[item.type] as any, { key: i, ...item });
        })}
      </Box>
    </Fullscreen>
  );
};

const Log = (props: { value: string }) => {
  return (
    <Box flexDirection='row'>
      <Text>{figures.bullet} </Text>
      <Text>{props.value}</Text>
    </Box>
  );
};

const Error = (props: { value: string }) => {
  return (
    <Box flexDirection='row'>
      <Text>{figures.bullet} </Text>
      <Text color='red'>{props.value}</Text>
    </Box>
  );
};

const Info = (props: { value: string }) => {
  return (
    <Box flexDirection='row'>
      <Text>{figures.bullet} </Text>
      <Text color='yellow'>{props.value}</Text>
    </Box>
  );
};

const TextInput = (props: { submit?: (value: string) => void; submited?: string }) => {
  const [input, setInput] = useState('');

  if (props.submited !== undefined) {

    return (
      <Box flexDirection='row'>
        <Text>{figures.bullet} </Text>
        <Text>{props.submited}</Text>
      </Box>
    );
  }
  return (
    <Box flexDirection='row'>
      <Text>{figures.pointer} </Text>
      <IncTextInput value={input} onChange={setInput} onSubmit={v => props.submit?.(v)} />
    </Box>
  );
};

const OutputViews = {
  log: Log,
  error: Error,
  info: Info,
  textInput: TextInput,
};

export default JobScreen;
