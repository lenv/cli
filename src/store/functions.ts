import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';

type CustomFunction = {
  name: string;
  interpolationPrefix: string;
  body: (params: any) => Promise<void>;
};
type State = CustomFunction[];

const setFunctions = (state, action: PayloadAction<CustomFunction[]>) => [...action.payload];

const functionsState = createSlice({
  name: 'functions',
  initialState: [] as CustomFunction[],
  reducers: {
    setFunctions,
  },
});

const functions = (state: { functions: State }) => state.functions;

export default {
  ...functionsState,
  selectors: { functions },
};
