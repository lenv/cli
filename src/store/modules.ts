import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';
import selectedConfigsState from './selectedConfigs';

type Config = {
  name: string;
  requires?: string[];
  jobs?: (string | [string, Record<string, any>])[];
  [key: string]: any;
};

type Module = {
  name: string;
  description: string;
  defaultConfig: string;
  configs: Config[];
};
type State = Module[];

const setModules = (state, action: PayloadAction<Module[]>) => [...action.payload];

const modulesState = createSlice({
  name: 'modules',
  initialState: [],
  reducers: {
    setModules,
  },
});

const modules = (state: { modules: State }) => state.modules;
const modulesNames = createSelector(modules, state => state.map(item => item.name));
const moduleByName = (moduleName: string) => createSelector(modules, state => state.find(M => M.name === moduleName));

const activeConfig = (moduleName: string) =>
  createSelector(
    selectedConfigsState.selectors.selectedConfigName(moduleName), //
    moduleByName(moduleName),
    (selectedConfigName, module) => {
      const configName = selectedConfigName ?? module?.defaultConfig;
      return (
        module?.configs.find(C => C.name === configName) ?? module?.configs.find(C => C.name === module?.defaultConfig)
      );
    },
  );

export default {
  ...modulesState,
  selectors: { modules, modulesNames, moduleByName, activeConfig },
};
