import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';

type Job = {
  name: string;
  stage: string;
  requires?: string[];
  jobs?: (string | [string, Record<string, any>])[];
  body: (params: any) => Promise<void>;
};
type State = Job[];

const setJobs = (state, action: PayloadAction<Job[]>) => [...action.payload];

const modulesState = createSlice({
  name: 'jobs',
  initialState: [],
  reducers: {
    setJobs,
  },
});

const jobs = (state: { jobs: State }) => state.jobs;
const job = (name: string) => createSelector(jobs, state => state.filter(job => job.name === name)?.[0]);
const stageJobs = (stage: string) => createSelector(jobs, state => state.filter(job => job.stage === stage));

export default {
  ...modulesState,
  selectors: { jobs, job, stageJobs },
};
