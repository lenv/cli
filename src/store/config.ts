import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';

type State = {
  runStages: string[];
  functions?: {
    [key: string]: (...args:any[]) => void;
  };
};

const setConfig = (state, action: PayloadAction<State>) => ({ ...action.payload });

const configState = createSlice({
  name: 'config',
  initialState: {
    runStages: ['build', 'run'],
  } as State,
  reducers: {
    setConfig,
  },
});

const config = (state: { config: State }) => state.config;
const runStages = createSelector(config, state => state.runStages);

export default {
  ...configState,
  selectors: { config, runStages },
};
