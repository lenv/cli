import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';
import { assocPath } from 'ramda';

type Output = { id?: string } & (
  | {
      type: 'log' | 'error' | 'info';
      value: string;
    }
  | {
      type: 'textInput';
      submit?: (value: string) => void;
      submited?: string;
    }
);

export type Status = 'paused' | 'running' | 'succeeded' | 'failed' | 'waiting';

type State = {
  status: { [jobName: string]: Status };
  output: { [jobName: string]: Output[] };
};

const pushOutput = (state: State, action: PayloadAction<{ jobName: string; output: Output }>) => {
  return {
    ...state,
    output: {
      ...state.output,
      [action.payload.jobName]: [
        ...(state.output?.[action.payload.jobName] ?? []), //
        action.payload.output,
      ],
    },
  };
};

const updateOutput = (state: State, action: PayloadAction<{ jobName: string; id: string; output: Output }>) => {
  return {
    ...state,
    output: {
      ...state.output,
      [action.payload.jobName]: (state.output?.[action.payload.jobName] ?? []).map(o => {
        if (o?.id === action.payload.id) {
          return action.payload.output;
        } else {
          return o;
        }
      }),
    },
  };
};

const clearOutput = (state: State) => {
  return {
    ...state,
    output: {},
  };
};

const updateStatus = (state: State, action: PayloadAction<{ jobName: string; status: Status }>) => {
  return {
    ...state,
    status: {
      ...state.status,
      [action.payload.jobName]: action.payload.status,
    },
  };
};

const clearStatuses = (state: State) => {
  return {
    ...state,
    status: {},
  };
};

const jobsStorageState = createSlice({
  name: 'jobsStorage',
  initialState: {},
  reducers: {
    pushOutput,
    clearOutput,
    updateStatus,
    clearStatuses,
    updateOutput,
  },
});

const jobsStorage = (state: { jobsStorage: State }) => state.jobsStorage;
const jobOutput = (jobName: string) =>
  createSelector(
    jobsStorage, //
    state => state.output?.[jobName] ?? [],
  );

const jobStatus = (jobName: string) =>
  createSelector(
    jobsStorage, //
    state => state.status?.[jobName] ?? 'paused',
  );

export default {
  ...jobsStorageState,
  selectors: { jobsStorage, jobOutput, jobStatus },
};
