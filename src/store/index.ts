import * as path from 'path';
import { combineReducers, configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import { AsyncNodeStorage } from 'redux-persist-node-storage';

import enabledModulesState from './enabledModules';
import routerState from './router';
import modulesState from './modules';
import jobsState from './jobs';
import jobsStorageState from './jobsStorage';
import jobsArtifactsState from './jobsArtifacts';
import functionsState from './functions';
import selectedConfigsState from './selectedConfigs';
import configState from './config';


const persistConfig = {
  key: 'root',
  storage: new AsyncNodeStorage(path.join(process.cwd(), '/state')),
};

const persistedReducer = persistReducer(
  persistConfig,
  combineReducers({
    enabledModules: enabledModulesState.reducer,
    selectedConfigs: selectedConfigsState.reducer,
    jobsArtifacts: jobsArtifactsState.reducer,
  }),
);

const reducer = combineReducers({
  router: routerState.reducer,
  modules: modulesState.reducer,
  jobs: jobsState.reducer,
  jobsStorage: jobsStorageState.reducer,
  functions: functionsState.reducer,
  config: configState.reducer,
  persist: persistedReducer,
});



const store = configureStore({
  reducer,
  middleware: getDefaultMiddleware({ serializableCheck: false }),
});

export const persistor = persistStore(store);

export default store;
