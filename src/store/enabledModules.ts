import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';
import { uniq } from 'ramda';
import modulesState from './modules';
import jobsState from './jobs';
import { buildJob } from '../utils/build';
import { runDockerJob } from '../utils/runDocker';

const add = (state, action: PayloadAction<string>) => uniq([...state, action.payload]);
const remove = (state, action: PayloadAction<string>) => state.filter(item => item !== action.payload);

const toggle = (state, action: PayloadAction<string>) =>
  state.includes(action.payload) ? remove(state, action) : add(state, action);

const enabledModulesState = createSlice({
  name: 'enabledModules',
  initialState: [],
  reducers: {
    add,
    remove,
    toggle,
  },
});

const enabledModules = createSelector(
  modulesState.selectors.modulesNames,
  (state: any) => state.persist.enabledModules,
  (availableModules, enabledModules) =>
    enabledModules.filter(enabledModule => availableModules.includes(enabledModule)),
);
const isEnabled = moduleName => createSelector(enabledModules, state => state.includes(moduleName));

type RequiredJob = string | [string, any] | { name: string; params: any };
const getJobName = (job: RequiredJob) => {
  if (typeof job === 'string') return job;
  if (Array.isArray(job)) return job[0];
  return job.name;
};
const getJobParams = (job: RequiredJob) => {
  if (typeof job === 'string') return {};
  if (Array.isArray(job)) return job[1];
  return job.params;
};

const dependencies = createSelector(
  state => state,
  enabledModules,
  (state, enabledModules) => {
    const depModules = [];
    const depJobs = { [buildJob.name]: { args: [] }, [runDockerJob.name]: { args: [] } };
    const stack: ({ type: 'module'; module: string } | { type: 'job'; job: string })[] = [
      ...enabledModules.map(m => ({ type: 'module', module: m })),
    ];

    while (stack.length > 0) {
      const item = stack.pop();

      let requires: string[];
      let jobs: (string | [string, Record<string, any>])[];
      if (item.type === 'module') {
        const selectedConfig = modulesState.selectors.activeConfig(item.module)(state);
        requires = selectedConfig?.requires ?? [];
        jobs = selectedConfig?.jobs ?? [];
      } else if (item.type === 'job') {
        const job = jobsState.selectors.job(item.job)(state);
        requires = job?.requires ?? [];
        jobs = job?.jobs ?? [];
      }

      requires.forEach(r => {
        if (!depModules.includes(r) && !enabledModules.includes(r)) {
          depModules.push(r);
          stack.unshift({ type: 'module', module: r });
        }
      });

      jobs.forEach(j => {
        const jobName = getJobName(j);
        const jobArg = getJobParams(j);

        if (!depJobs[jobName]) {
          depJobs[jobName] = { args: [jobArg] };
          stack.unshift({ type: 'job', job: jobName });
        } else {
          depJobs[jobName].args.push(jobArg);
        }
      });
    }

    return { modules: depModules, jobs: depJobs };
  },
);

export default {
  ...enabledModulesState,
  selectors: { enabledModules, isEnabled, dependencies },
};
