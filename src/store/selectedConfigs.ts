import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';

type State = Record<string, string>;

const set = (state, action: PayloadAction<{ moduleName: string; configName: string }>) => ({
  ...state,
  [action.payload.moduleName]: action.payload.configName,
});

const selectedConfigsState = createSlice({
  name: 'selectedConfigs',
  initialState: {},
  reducers: {
    set,
  },
});

const selectedConfigs = (state: { persist: { selectedConfigs: State } }) => state.persist.selectedConfigs;
const selectedConfigName = (moduleName: string) =>
  createSelector(
    selectedConfigs, //
    configs => {
      return configs[moduleName];
    },
  );

export default {
  ...selectedConfigsState,
  selectors: { selectedConfigs, selectedConfigName },
};
