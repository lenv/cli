import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';

type StateItem = { route: string; params?: Record<string, unknown> };
type State = StateItem[];

const push = (state: State, action: PayloadAction<StateItem>) => [...state, action.payload];
const pop = (state: State) => state.slice(0, state.length - 1);

const routerState = createSlice({
  name: 'router',
  initialState: [
    {
      route: 'Home',
    },
  ],
  reducers: {
    push,
    pop,
  },
});

const history = (state: { router: State }) => state.router;
const getPath = createSelector(history, (state: State) => state.map(s => s.route).join('.'));

export default {
  ...routerState,
  selectors: {
    history,
    getPath
  },
};
