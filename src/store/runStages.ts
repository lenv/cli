import { createSelector } from 'reselect';
import configState from './config';
import enabledModulesState from './enabledModules';
import jobsStorageState from './jobsStorage';
import jobsArtifactsState from './jobsArtifacts';
import jobsState from './jobs';

const activeRunStages = createSelector(
  configState.selectors.runStages,
  enabledModulesState.selectors.dependencies,
  jobsState.selectors.jobs,
  jobsArtifactsState.selectors.jobsArtifacts,
  (stages, dependencies, jobs, artifacts) => {
    const activeJobs = dependencies.jobs;

    return stages
      .map(stage => {
        return {
          name: stage,
          jobs: jobs
            .filter(job => job.stage === stage && activeJobs[job.name])
            .map(job => ({
              name: job.name,
              body: job.body,
              artifacts: artifacts?.[job.name] ?? {},
              args: activeJobs[job.name].args,
            })),
        };
      })
      .filter(stage => stage.jobs.length > 0);
  },
);

export default { selectors: { activeRunStages } };
