import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { createSelector } from 'reselect';

type State = { [jobName: string]: { [artifactName: string]: string } };

const updateArtifacts = (
  state: State,
  action: PayloadAction<{ jobName: string; artifacts: { [artifactName: string]: string } }>,
) => {
  return {
    ...state,
    [action.payload.jobName]: {
      ...state?.[action.payload.jobName],
      ...action.payload.artifacts,
    },
  };
};

const deleteArtifacts = (
  state: State,
  action: PayloadAction<{ jobName: string }>,
) => {
  return {
    ...state,
    [action.payload.jobName]: {},
  };
};

const jobsArtifactsState = createSlice({
  name: 'jobsArtifacts',
  initialState: {},
  reducers: {
    updateArtifacts,
    deleteArtifacts,
  },
});

const jobsArtifacts = (state: { persist: { jobsArtifacts: State } }) => state.persist.jobsArtifacts;


export default {
  ...jobsArtifactsState,
  selectors: { jobsArtifacts },
};
