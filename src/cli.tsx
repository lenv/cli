#!/usr/bin/env node
import React from 'react';
import { render } from 'ink';
import clearTerminal from './utils/clearTerminal';
import App from './App';

clearTerminal();
render(<App />);
