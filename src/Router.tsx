import React from 'react';
import { useSelector } from 'react-redux';
import { Box } from 'ink';

import routerState from './store/router';

import Home from './screens/HomeScreen';
import SelectConfigScreen from './screens/SelectConfigScreen';
import JobScreen from './screens/JobScreen';
import ContextScreen from './screens/ContextScreen';
import ArtifactsScreen from './screens/ArtifactsScreen';


const routes = {
  Home: Home,
  SelectConfigScreen,
  JobScreen,
  ContextScreen,
  ArtifactsScreen,
};

const Router = () => {
  const history = useSelector(routerState.selectors.history);

  return (
    <>
      {history.map((item, i) => {
        const Component = routes[item.route];
        if (!Component) throw new Error(`Route ${item.route} not found`);
        const foreground = i === history.length - 1;

        return (
          <Box key={i} display={foreground ? 'flex' : 'none'} flexDirection='column'>
            {<Component navigation={{ params: item.params, foreground }} />}
          </Box>
        );
      })}
    </>
  );
};

export default Router;
